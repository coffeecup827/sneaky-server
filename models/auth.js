const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const authSchema = new Schema({
  id_token : {
    type : String,
    required : true
  },
  socketId : {
    type : String,
    required : true,
    index : true
  },
  deviceId : {
    type : Schema.Types.ObjectId
  },
  session : {
    type : String,
    required : true
  }
})

module.exports = authSchema;
