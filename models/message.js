const mongoose = require('mongoose');
const {DecryptMessage} = require('../helpers/crypto')

const Schema = mongoose.Schema;
const messagePreferenceSchema = require('./messagePreference')

const messageSchema = new Schema({
  byDeviceId : {
    type : Schema.Types.ObjectId,
    required : true
  },
  messageCipherText : {
    type : String,
    required : true,
    minlength : 1,
    maxlength : 2016
  },
  messagePreference : {
      type : [messagePreferenceSchema],
      required : true
  },
  isReadByAll : {
    type : Boolean,
    default : false
  },
  createdAt : {
      type : Date,
      required : true
  }
})

messageSchema.statics.FetchMessages = FetchMessages;

let Messages = mongoose.model('Message', messageSchema);

async function FetchMessages(criteria,skipto,limit,sub){
    return await this.find(criteria).sort({createdAt : -1}).skip(skipto).limit(limit)
    .then(async (messages) => {
      //coz reversing array is much faster than sorting :p
      let sortedMessages = [];
      for (let index = (messages.length - 1); index >= 0; index--) {
        let message = messages[index].toObject({virtuals: true});

        message.message = await DecryptMessage(message.messageCipherText,sub);

        delete message.messageCipherText;
        sortedMessages.push(message)
      }

      return sortedMessages;
    });
}

module.exports = Messages;