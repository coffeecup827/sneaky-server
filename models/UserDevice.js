const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schemaModules = require('../helpers/schemaHelpers');
const devicePreferenceSchema = require('./devicePreference');

const userDeviceSchema = new Schema({
  DeviceName: {
    type: String,
    required: true
    },
  Type: {
    type: String,
    required: true
  },
  IsLoggedIn: {
    type: Boolean,
    required: true
  },
  UserId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  IsActive: {
    type: Boolean,
    required: true
  },
  Preferences : {
    type : [devicePreferenceSchema],
    required : true
  }
},{
  timestamps: true
});

userDeviceSchema.statics.CheckAndDrop = schemaModules.CheckAndDrop;
userDeviceSchema.statics.UserDeviceCount = UserDeviceCount;
userDeviceSchema.statics.Devices = Devices;
userDeviceSchema.statics.IsDeviceExist = IsDeviceExist;
userDeviceSchema.statics.GetDevicePreferences = GetDevicePreferences;
userDeviceSchema.statics.VerifyDeviceSession = VerifyDeviceSession;

userDeviceSchema.index({DeviceName : 1, UserId : 1},{unique : true});

let UserDevices = mongoose.model('UserDevice', userDeviceSchema);

module.exports = UserDevices;

//Helpers

async function UserDeviceCount(userId) {
  let count = await this.count({UserId : userId});
  return count;
}

async function IsDeviceExist(userId,deviceName) {
  let count = await this.count({UserId : userId,DeviceName : deviceName});
  return count;
}

async function VerifyDeviceSession(userId,deviceId,session) {
  let count = await Repository.Users.count({
    "_id" : userId,
    "ActiveSessions" : {
      $elemMatch: {
        "deviceId" : deviceId,
        "session" : session
      }
    }
  });
  return count;
}

async function Devices(userId) {
  return await this.find({UserId : userId}).select({UserId : 0,Preferences : 0,IsActive : 0,updatedAt: 0,createdAt : 0});
}

async function GetDevicePreferences(deviceId){

  let preferences = await this.findOne({_id : deviceId}).lean().select({Preferences : 1});
  let deviceIds = preferences.Preferences.map((p) => (p.DeviceId));

  let devices = await this.find({_id : {$in : deviceIds},IsActive : true}).lean()
    .select({DeviceName : 1,IsLoggedIn : 1,Type : 1, _id : 1});

  let devicePreference = preferences.Preferences.map((p) => {
    var device = devices.filter((d) => d._id.toString() == p.DeviceId.toString())[0];
    return {
    PreferenceId : p._id,
    DeviceId : p.DeviceId,
    DeviceName : device.DeviceName,
    IsLoggedIn : device.IsLoggedIn,
    Type : device.Type,
    IsIncluded : p.IsIncluded
  }});

  return devicePreference;
}
