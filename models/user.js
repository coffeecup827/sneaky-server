const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schemaModules = require('../helpers/schemaHelpers');
const Auth = require('./auth');

const userSchema = new Schema({
  Sub: {
    type: String,
    required: true,
	  unique : true
    },
  Auth : {
    type : [Auth]
  },
  ExpiredSessions : {
    type : [new Schema({
      session : {
        type: String
      }
    })]
  },
  ActiveSessions : {
    type : [new Schema({
      session : {
        type: String,
        required : true
      },
      deviceId : {
        type : Schema.Types.ObjectId
      }
    })]
  }
},{
  timestamps: true
});

userSchema.statics.CheckAndDrop = schemaModules.CheckAndDrop;
userSchema.statics.IsUserExist = IsUserExist;
userSchema.statics.CanUserSignIn = CanUserSignIn;

userSchema.virtual('UserId').get(function () {
  return this._id;
});

var Users = mongoose.model('User', userSchema);

module.exports = Users;


//Helpers

async function IsUserExist(sub) {
  return await this.findOne({Sub : sub});
}

async function CanUserSignIn(sub,id_token,session){
  let isTokenUsed = await this.count({
    "Sub" : sub,
    "Auth.id_token" : id_token
  });

  let oldActiveSession = await this.count({
    "Sub" : sub,
    "Auth.id_token" : {
      "$ne" : id_token
    },
    "ActiveSessions.session" : session
  });

  return {
    login : isTokenUsed === 0 || oldActiveSession  === 1,
    oldActiveSession : oldActiveSession  === 1
  };
}
