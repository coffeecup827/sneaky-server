const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schemaModules = require('../helpers/schemaHelpers');

const devicePreferenceSchema = new Schema({
  DeviceId : {
    type : Schema.Types.ObjectId,
    required : true
  },
  IsIncluded : {
    type : Boolean,
    required : true
  }
})

module.exports = devicePreferenceSchema;
