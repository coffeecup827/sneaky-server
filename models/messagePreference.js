const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const messagePreferenceSchema = new Schema({
  DeviceId : {
    type : Schema.Types.ObjectId,
    required : true
  },
  IsReceived : {
    type : Boolean,
    required : true
  },
  ReceivedAt : {
    type : Date
  }
})

module.exports = messagePreferenceSchema;
