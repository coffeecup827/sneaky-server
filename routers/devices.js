const express = require('express');
const bodyParser = require('body-parser');
const status = require('../helpers/status');
const validator = require('../helpers/validator');
const { matchedData } = require('express-validator/filter');
const Middleware = require('../helpers/middleware');

const devicesRouter = express.Router();
devicesRouter.use(bodyParser.json());

devicesRouter.route('/create')
.post(validator.CreateDeviceValidator,Middleware.VerifyDeviceNotSelected, async function (request, res) {

  const req = matchedData(request);

  let userData = request.SessionData.userData

  const user = request.SessionData.user

  let device = req.device;

  if (await Repository.UserDevices.UserDeviceCount(user._id) >= 5) {
    res.send(Helpers.RequestFailed("Device Limit Exhausted"));
    return;
  }

  if (await Repository.UserDevices.IsDeviceExist(user._id,device.DeviceName) !== 0) {
    res.send(Helpers.RequestFailed("Device Name Must Be Unique"));
    return;
  }

  let devices = await Repository.UserDevices.Devices(user._id);

  let devicePreferences = devices.map((device) => ({
    DeviceId : device._id,
    IsIncluded : true
  }));

  let newDevice = Repository.UserDevices({
      DeviceName : device.DeviceName,
      Type : device.Type,
      IsLoggedIn : false,
      IsActive : true,
      UserId : user._id,
      Preferences : devicePreferences
  });

  newDevice.save().then(async function(device) {

    let deviceIds = devices.map(x => x._id);
    var criteria = {
      _id : {
        $in : deviceIds
      }
    }

    await Repository.UserDevices.updateMany(criteria,{
      $push: {
        'Preferences': {
          DeviceId : device._id,
          IsIncluded : true
        }
      }
    });

    return device;
  }).then(async function(device){

    await Repository.Users.updateMany(
    {
      Sub : userData.sub,
      "Auth.session": req.session
    },
    {
      $set : {
        "Auth.$.deviceId" : device._id
      }
    })

    await Repository.Users.update(
    {
      Sub : userData.sub,
      "ActiveSessions.session" : req.session
    },
    {
      $set : {
        "ActiveSessions.$.deviceId" : device._id
      }
    })

    let date = new Date();
    date.setDate(date.getDate() + 7);
    res.cookie('CurrentDevice', device._id, {expires: date});

    let preferences = await Repository.UserDevices.GetDevicePreferences(device._id);

    let emitData = {
      [device._id] : {
        _id : device._id,
        DeviceName : device.DeviceName,
        Type : device.Type,
        IsLoggedIn : device.IsLoggedIn
      }
    }
    let messageTemplate = Helpers.StringTemplate`New SneakBot ${'deviceName'} Added`;
    Helpers.NotifyUserDevicesExceptSender('new-device-added',device._id,emitData,messageTemplate);

    res.send(JSON.stringify({
      "status": status.Success,
      "data": {
        DeviceId : device._id,
        DeviceName : device.DeviceName,
        Type : device.Type,
        IsLoggedIn : device.IsLoggedIn,
        Preferences : preferences
      },
      "message": "Device Added"
    }));
    return;
  }).catch((err) => {
    res.send(Helpers.RequestFailed("Couldn't Create Device."));
  });

});

devicesRouter.route('/select')
.post(validator.SelectDeviceValidator,Middleware.VerifyDeviceNotSelected, async function (request,res){

  const req = matchedData(request);

  let userData = request.SessionData.userData

  let device = req.device;

  let isAlreadyLoggedIn = await Repository.Users.findOne({
    Sub : userData.sub,
    "ActiveSessions.deviceId" : device.DeviceId
  });

  if (isAlreadyLoggedIn) {
    return res.send(Helpers.RequestFailed("Device Already Selected"));
  }

  let devicePreferences = await Repository.UserDevices.GetDevicePreferences(device.DeviceId);

  await Repository.Users.updateMany(
    {
      Sub : userData.sub
    },
    {
      $set : {
        "Auth.$[auth].deviceId" : device.DeviceId
      }
    },
    { 
      arrayFilters: [{ 
        "auth.session": { $eq: req.session }
      }]
    }).then(async () => {

      await Repository.Users.update(
        {
          Sub : userData.sub,
          "ActiveSessions.session" : req.session
        },
        {
          $set : {
            "ActiveSessions.$.deviceId" : device.DeviceId
          }
        })

      let expiryDate = new Date();
      expiryDate.setDate(expiryDate.getDate() + 7);
      res.cookie('CurrentDevice', device.DeviceId, {expires: expiryDate});

      res.send(JSON.stringify({
        "status": status.Success,
        "data": {
          DeviceId : device.DeviceId,
          DeviceName : device.DeviceName,
          Preferences : devicePreferences
        },
        "message": "Sneak Bot : " + device.DeviceName
      }));
    }).catch((err) => {
      return res.send(Helpers.RequestFailed("Couldn't Select Device"));
    });
})

devicesRouter.route('/userDevices')
.post(validator.GetUserDevicesValidator,Middleware.VerifyUserSession, async function(request,res){

  const req = matchedData(request);

  let userData = request.SessionData.userData

  const user = request.SessionData.user;
  userData.Devices = await Repository.UserDevices.Devices(user._id);
  res.send(JSON.stringify({
    "status": status.UserExist,
    "data": userData,
    "message": ""
  }));
});

devicesRouter.route('/preferences')
.post(validator.GetDevicePreferences,Middleware.VerifyUserDeviceSession, async function (request,res){

  const req = matchedData(request);

  let userData = request.SessionData.userData

  let deviceId = req.deviceId;

  let devicePreferences = await Repository.UserDevices.GetDevicePreferences(deviceId);

  res.send(JSON.stringify({
    "status": status.Success,
    "data": {
      DeviceId : deviceId,
      Preferences : devicePreferences
    },
    "message": ""
  }));
})
.put(validator.UpdateDevicePreferences,Middleware.VerifyUserDeviceSession, async function (request,res){

  const req = matchedData(request);

  let deviceId = req.deviceId;
  let preferenceId = req.preferenceId;
  let isIncluded = req.isIncluded;

  let failedReturnData = {
    preference : {
      deviceId,
      preferenceId,
      isIncluded : isIncluded == "true" ? "false" : "true"
    }
  }

  await Repository.UserDevices.findOneAndUpdate(
    {
      _id : deviceId,
      "Preferences._id": preferenceId
    },
    {
      $set : {
        "Preferences.$.IsIncluded" : isIncluded
      }
    }
  ,function(err,doc){
    let returnData = {
      preference : {
        deviceId,
        preferenceId,
        isIncluded
      }
    }

    if (err) {
      res.send(Helpers.UpdateFailed("Couldn't Update Device Preference",failedReturnData));
      return;
    }

    let senderMessageTemplate = Helpers.StringTemplate``;
    Helpers.NotifySenderDevice('update-device-preference',deviceId,returnData,senderMessageTemplate);

    res.send(JSON.stringify({
      "status": status.Success,
      "data": returnData,
      "message": "Preference Updated"
    }));
  })
})

module.exports = devicesRouter;
