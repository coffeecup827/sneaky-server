const express = require('express');
const bodyParser = require('body-parser');
const status = require('../helpers/status');
const validator = require('../helpers/validator');
const { validationResult } = require('express-validator/check');
const { matchedData } = require('express-validator/filter');
const Crypto = require('../helpers/crypto');
const Middleware = require('../helpers/middleware');

const userRouter = express.Router();
userRouter.use(bodyParser.json());


userRouter.route('/signin')
.post(validator.SignInValidator, async function (request, res) {

  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return res.send(Helpers.RequestFailed("Invalid Data Sent"));
  }
  const req = matchedData(request);

  var userData = await Helpers.VerifyUserToken(req.id_token)
                              .catch(console.error);

  if (userData) {
    signinUser(userData,req, res);
  }
  else {
    res.send(Helpers.RequestFailed("Couldn't Signin"));
  }
});

userRouter.route('/logout')
.post(validator.SignOutValidator,Middleware.VerifyUserSession, async function (request, res) {

  const req = matchedData(request);
  data = request.SessionData;

  var sessionDetail = await Repository.Users.findOne({
    Sub : data.userData.sub,
    "ActiveSessions" : {
      $elemMatch : {
        "session" : req.session
      }
    }
  },{
    "ActiveSessions.$.deviceId" : 1
  })

  Repository.Users.findOneAndUpdate({
    Sub : data.userData.sub,
    "Auth" : {
      $elemMatch : {
        "session" : req.session
      }
    },
    "ActiveSessions" : {
      $elemMatch : {
        "session" : req.session
      }
    }
  },{
    $pull : {
      Auth : {
        "session" : req.session
      },
      ActiveSessions : {
        "session" : req.session
      }
    },
    $push: { 
      ExpiredSessions: {
        session : req.session
      }
    }
  }).then(async () => {
    
    if (sessionDetail.ActiveSessions[0].deviceId) {
      let deviceId = sessionDetail.ActiveSessions[0].deviceId;
      await Repository.UserDevices.findOneAndUpdate({
        _id : deviceId
      },{
        IsLoggedIn : false
      })
      let data = {
        deviceId : deviceId,
        IsLoggedIn : false
      }
      let messageTemplate = Helpers.StringTemplate`${'deviceName'} Offline`;
      Helpers.NotifyUserDevicesExceptSender('device-status',deviceId,data,messageTemplate);
    }

    res.clearCookie('CurrentDevice');
    res.clearCookie('session');
    res.send(JSON.stringify({
      "status": status.Success,
      "data": null,
      "message": "Logged Out Successfully"
    }));
  }).catch((err) => {
    console.log(err)
    console.log(req)
    res.send(Helpers.RequestFailed("Couldn't Logout"));
  });

});

async function signinUser(userData,req,res) {
  
  const CurrentDevice = req.CurrentDevice;

  const user = await Repository.Users.findOne({
    "Sub" : userData.sub
  });

  let expiryDate = new Date();
  expiryDate.setDate(expiryDate.getDate() + 7);

  let canUSerSignInData = await Repository.Users.CanUserSignIn(userData.sub,req.id_token,req.session);

  if (user && canUSerSignInData.login) {

    userData.Devices = await Repository.UserDevices.Devices(user._id);
    
    let session = req.session ? req.session : (new Date()).getTime().toString();
    userData.session = session;

    if (CurrentDevice !== undefined && await Repository.UserDevices.VerifyDeviceSession(user._id,CurrentDevice,req.session)) {
        await Repository.UserDevices.findOne({_id : CurrentDevice}).then(async function(device) {
          userData.CurrentDevice =  CurrentDevice;
          userData.DeviceName = device.DeviceName;
          userData.Preferences =  await Repository.UserDevices.GetDevicePreferences(CurrentDevice);
        });
    }
    else{
      userData.CurrentDevice =  null;
      userData.DeviceName = null;
      userData.Preferences = [];
    }

    user.Auth.push({
      id_token : req.id_token,
      socketId : req.socketId,
      session : session,
      deviceId : CurrentDevice
    })

    if (!canUSerSignInData.oldActiveSession) {
      user.ActiveSessions.push({
        session : session
      })
    }

    user.save().then(() => {
        res.cookie('session', session, {expires: expiryDate});

        res.send(JSON.stringify({
          "status": status.UserExist,
          "data": userData,
          "message": "Welcome Sneaker"
        }));
      })
      .catch(() => {
        res.send(Helpers.RequestFailed("Couldn't Sign In"));
      });
    return;
  }

  let session = (new Date()).getTime().toString();
  userData.session = session;

  let newUser = Repository.Users({
      Sub: userData.sub,
      ExpiredSessions : []
  });
  newUser.Auth.push({
    id_token : req.id_token,
    socketId : req.socketId,
    session : session
  })

  newUser.ActiveSessions.push({
    session : session
  })

  newUser.save()
        .then((user) => {
          userData.Devices = [];
          userData.CurrentDevice = null;
          userData.DeviceName = null;
          userData.Preferences = [];

          res.cookie('session', session, {expires: expiryDate});

          res.send(JSON.stringify({
            "status": status.UserCreated,
            "data": userData,
            "message": "Yay! Your'e a Sneaker now"
          }));
        })
        .catch((err) => {
          console.log(err)
          console.log(req)
          res.send(Helpers.RequestFailed("User Creation Failed"));
        });
}

module.exports = userRouter;
