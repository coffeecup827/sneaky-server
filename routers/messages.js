const express = require('express');
const bodyParser = require('body-parser');
const status = require('../helpers/status');
const validator = require('../helpers/validator');
const { matchedData } = require('express-validator/filter');
const Middleware = require('../helpers/middleware');
const {EncryptMessage} = require('../helpers/crypto')

const messagesRouter = express.Router();
messagesRouter.use(bodyParser.json());


messagesRouter.route('/sneakbox')
.post(validator.GetMessageValidator,Middleware.VerifyUserDeviceSession, async function(request,res){
  const req = matchedData(request);

  let device = request.SessionData.device;
  let user = request.SessionData.user;

  var criteria = {
    $or : [{
      byDeviceId : device._id
    },{
      "messagePreference.DeviceId" : device._id
    }]
  };

  Repository.Messages.FetchMessages(criteria,req.skipTo,5,user.Sub)
  .then(async (messages) => {
    res.send(JSON.stringify({
      "status": status.Success,
      "data": {
        "messages" : messages
      },
      "message": ""
    }));
  }).catch(() => {
    res.send(Helpers.RequestFailed("Couldn't Update Sneaks"));
  })

})

messagesRouter.route('/inbox')
.post(validator.GetMessageValidator,Middleware.VerifyUserDeviceSession, async function(request,res){
  const req = matchedData(request);

  let device = request.SessionData.device;
  let user = request.SessionData.user;

  var criteria = {
    "messagePreference.DeviceId" : device._id
  }

  Repository.Messages.FetchMessages(criteria,req.skipTo,5,user.Sub)
  .then((messages) => {
    res.send(JSON.stringify({
      "status": status.Success,
      "data": {
        "messages" : messages
      },
      "message": ""
    }));
  }).catch(() => {
    res.send(Helpers.RequestFailed("Couldn't Update Sneaks"));
  })

})

messagesRouter.route('/sent')
.post(validator.GetMessageValidator,Middleware.VerifyUserDeviceSession, async function(request,res){
  const req = matchedData(request);

  let device = request.SessionData.device;
  let user = request.SessionData.user;

  var criteria = {
      "byDeviceId" : device._id
  }

  Repository.Messages.FetchMessages(criteria,req.skipTo,5,user.Sub)
  .then((messages) => {
    res.send(JSON.stringify({
      "status": status.Success,
      "data": {
        "messages" : messages
      },
      "message": ""
    }));
  }).catch(() => {
    res.send(Helpers.RequestFailed("Couldn't Update Sneaks"));
  })

})

messagesRouter.route('/send')
.put(validator.SendMessageValidator,Middleware.VerifyUserDeviceSession,async function(request, res){

  const req = matchedData(request);

  if(req.message.trim() === "") res.send(Helpers.RequestFailed("Sneak Is Empty"));

  let device = request.SessionData.device;

  let messagePreferences = device.Preferences.filter(x => x.IsIncluded).map(x => ({
    DeviceId : x.DeviceId,
    IsReceived : false
  }))

  let newMessage = Repository.Messages({
    byDeviceId : device._id,
    messageCipherText : await EncryptMessage(req.message,request.SessionData.user.Sub),
    messagePreference : messagePreferences,
    createdAt : Date.now()
  });

  newMessage.save().then(async (Emessage) => {

    let message = Emessage.toObject();
    message.message = req.message;
    delete message.messageCipherText;

    let emitData = {
      message : message
    }
    let messageTemplate = Helpers.StringTemplate`${'deviceName'} Sent A Sneak`;
    Helpers.NotifyUserDevicesExceptSender('message-received',device._id,emitData,messageTemplate,false);

    let senderMessageTemplate = Helpers.StringTemplate``;
    Helpers.NotifySenderDevice('message-sent',device._id,emitData,senderMessageTemplate,false,null,req.id_token);

    res.send(JSON.stringify({
      "status": status.Success,
      "data": {
        message : message
      },
      "message": "Sneak Sent"
    }));
  }).catch((err) => {
    console.log(err);
    
    res.send(Helpers.RequestFailed("Unable To Send Sneak"));
  })
})

messagesRouter.route('/read')
.put(validator.MessageReceivedValidator,Middleware.VerifyDeviceMessageAndSession,async function(request, res){

  const req = matchedData(request);
  
  let currentTime = Date.now();

  let messagePreference = request.SessionData.message.messagePreference;                                             //since we're checking before updating
  let isReadByAll = messagePreference.filter(x => x.IsReceived).length === (messagePreference.length - 1);

  Repository.Messages.findOneAndUpdate({
    _id : req.messageId,
    "messagePreference.DeviceId" : req.deviceId
  },{
    $set : {
      "messagePreference.$.IsReceived" : true,
      "messagePreference.$.ReceivedAt" : currentTime,
    },
    "isReadByAll" : isReadByAll
  }).then(() => {

    let messageData = {
      deviceId : req.deviceId,//used to identify the message prefrence in client
      ReceivedAt : currentTime,
      IsReceived : true,
      messageId : req.messageId,
      isReadByAll
    }

    let messageTemplate = Helpers.StringTemplate``;
    Helpers.NotifyUserDevicesExceptSender('update-message-preference',req.deviceId,messageData,messageTemplate,false);

    let senderMessageTemplate = Helpers.StringTemplate``;
    Helpers.NotifySenderDevice('update-message-preference',req.deviceId,messageData,senderMessageTemplate);

    res.send(JSON.stringify({
      "status": status.Success,
      "data": {
        messageData
      },
      "message": "Working"
    }));
  })
  .catch(() => {
    res.send(Helpers.RequestFailed(""));
  })
  
  

})

module.exports = messagesRouter;