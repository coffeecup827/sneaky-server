const express = require('express');
const http = require('http');
const cookieParser = require('cookie-parser');

const user = require('./routers/user');
const deviceRouter = require('./routers/devices');
const messagesRouter = require('./routers/messages');
global.Repository = require('./helpers/repository');
global.Helpers = require('./helpers/globals');
const socketHandler = require('./helpers/socket');

process.on('SIGINT',function(){
  Repository.Connect.then((db) => {
    db.disconnect();
  });
});

const app = express();
app.use(cookieParser());
const server = http.createServer(app);

global.io = require('socket.io')(server,{transports: ['websocket']});
io.on('connection', socketHandler);

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', (process.env.PORT ? 'https://sneaky-dev.herokuapp.com' : 'http://localhost:3000'));
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, DELETE, PUT');
    next();
});

app.use('/user/',user);
app.use('/devices/',deviceRouter);
app.use('/messages/',messagesRouter);

server.listen(process.env.PORT || 300,() => {
  console.log("Server running at localhost:" + (process.env.PORT || 300));
});
