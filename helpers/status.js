module.exports = {
  Success : "Success",
  Failed : "Failed",
  UserExist : "UserExist",
  BadRequest : "BadRequest",
  UserCreated : "UserCreated",
  UpdateFailed : "UpdateFailed",
  SessionExpired : "SessionExpired"
}
