const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const url = process.env.PORT ? process.env.MONGODB_URI : 'mongodb://localhost:27017/sneaky';
const connect = mongoose.connect(url, {});

const repository = {
  Users : require('../models/user'),
  UserDevices : require('../models/UserDevice'),
  Messages : require('../models/message'),
  Connect : connect
}

module.exports = repository;
