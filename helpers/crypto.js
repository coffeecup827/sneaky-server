const crypto = require('crypto');

module.exports = {
  EncryptMessage : EncryptMessage,
  DecryptMessage : DecryptMessage
}

function EncryptMessage(message,sub){

  const hashCipher = crypto.createCipher('aes192', (process.env.HashKey ? process.env.HashKey : 'I\'m the hash key'));
  let hash = hashCipher.update(sub, 'utf8', 'hex')
  hash += hashCipher.final('hex');

  let messageCipher = crypto.createCipher('aes192', hash);
  let messageHash = messageCipher.update(message, 'utf8', 'hex')
  messageHash += messageCipher.final('hex');

  return messageHash;
}


function DecryptMessage(messageHash,sub){

  const hashCipher = crypto.createCipher('aes192', (process.env.HashKey ? process.env.HashKey : 'I\'m the hash key'));
  let hash = hashCipher.update(sub, 'utf8', 'hex')
  hash += hashCipher.final('hex');

  let messageDecipher = crypto.createDecipher('aes192', hash);
  let message = messageDecipher.update(messageHash, 'hex', 'utf8')
  message += messageDecipher.final('utf8');

  return message;
}