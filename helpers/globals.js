const status = require('./status');
const {OAuth2Client} = require('google-auth-library');
const credentials = require('../credentials.json').web;

const Helpers = {
  RequestFailed : RequestFailed,
  UpdateFailed : UpdateFailed,
  SessionExpired : SessionExpired,
  VerifyUserToken : VerifyUserToken,
  VerifyUserSession : VerifyUserSession,
  VerifyUserDeviceSession : VerifyUserDeviceSession,
  StringTemplate : StringTemplate,
  NotifyUserDevicesExceptSender : NotifyUserDevicesExceptSender,
  NotifySenderDevice : NotifySenderDevice
}

module.exports = Helpers;

function RequestFailed(messgae){
  return JSON.stringify({
    "status": status.Failed,
    "data": null,
    "message": messgae
  });
}

function SessionExpired(){
  return JSON.stringify({
    "status": status.SessionExpired,
    "data": null,
    "message": "Your Session Expired"
  });
}

function UpdateFailed(messgae,data){
  return JSON.stringify({
    "status": status.UpdateFailed,
    "data": data,
    "message": messgae
  });
}

async function VerifyUserSession(id_token,session){
  const client = new OAuth2Client(credentials.client_id);

  const ticket = await client.verifyIdToken({
      idToken: id_token,
      audience: credentials.client_id,
  });
  const userData = ticket.getPayload();

  let user = await Repository.Users.findOne({
    "Sub" : userData.sub,
    "Auth" : {
      $elemMatch: {
        "id_token" : id_token,
        "session" : session
      }
    }
  });

  if (userData && user) {
    return {
      userData,
      user
    };
  }

  return null;
}

async function VerifyUserDeviceSession(id_token,session,deviceId){
  const client = new OAuth2Client(credentials.client_id);

  const ticket = await client.verifyIdToken({
      idToken: id_token,
      audience: credentials.client_id,
  });
  const userData = ticket.getPayload();

  let user = await Repository.Users.findOne({
    "Sub" : userData.sub,
    "Auth" : {
      $elemMatch: {
        "id_token" : id_token,
        "session" : session,
        "deviceId" : deviceId
      }
    }
  });

  if (userData && user) {

    let device = await Repository.UserDevices.findOne({
      UserId :user._id,
      _id : deviceId
    })
    return {
      userData,
      user,
      device
    };
  }

  return null;
}

async function VerifyUserToken(id_token){
  const client = new OAuth2Client(credentials.client_id);

  const ticket = await client.verifyIdToken({
      idToken: id_token,
      audience: credentials.client_id,
  });
  const userData = ticket.getPayload();

  return userData;
}

function StringTemplate(strings,...keys){
  return function(...values){
      var valuesDict = values[0] || {};

      var result = [];

      if (strings[0] !== "") {
        result.push(strings[0])
      }

      keys.forEach(function(key, i) {
        let value =valuesDict[key];
        result.push(value,strings[i + 1]);
      });

      return result.join('');
  }
}

async function NotifyUserDevicesExceptSender(event,deviceId,data,messageTemplate,sendToExcluded = true){
  
  let deviceData = await Repository.UserDevices.findOne({
    _id : deviceId
  },{
    Preferences : 1,
    DeviceName : 1
  })
  
  let message = messageTemplate({deviceName : deviceData.DeviceName});

  let emitMessage = {
    "status": status.Success,
    "data": data,
    "message": message
  }

  if (sendToExcluded) {
    deviceData.Preferences.forEach(device => {
      io.to(device.DeviceId).emit(event, emitMessage);
    });
  }
  else{
    deviceData.Preferences.forEach(device => {
      if (!device.IsIncluded) return;
      io.to(device.DeviceId).emit(event, emitMessage);
    });
  }
}

async function NotifySenderDevice(event,deviceId,data,messageTemplate,includeSender = true,socket=null,id_token=null){
  //send the message noramlly on include sender and socket is known
  //when socket is unkonwn and you want to reject event to sender, it includes the socket id in response,
  //so you can neglect the event in client side
  let deviceData = await Repository.UserDevices.findOne({
    _id : deviceId
  },{
    DeviceName : 1
  })

  let message = messageTemplate({deviceName : deviceData.DeviceName});

  let emitMessage = {
    "status": status.Success,
    "data": data,
    "message": message
  }
  
  if (includeSender) {
    io.to(deviceId).emit(event, emitMessage);
  }
  else{
    if (socket) {
      socket.to(deviceId).emit(event, emitMessage);
    }
    else{
      await Repository.Users.aggregate([{
        $match : {
          "Auth" : {
            $elemMatch : {
              "id_token" : id_token
            }
          }
        }
      },{
        $project :{
          "Auth" : {
            $filter: {
              input: "$Auth",
              as : "auth",
              cond: { $eq: [ "$$auth.id_token", id_token ] }
           }
          }
        }
      }]).then((user) => {

        let socketId = user[0].Auth[0].socketId.toString();

        emitMessage.data.socketId = socketId;
        
        io.to(deviceId).emit(event, emitMessage);
        
      }).catch((err) => {
        console.log(err);
        
      });
    }
  }
}