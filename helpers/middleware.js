const { validationResult } = require('express-validator/check');
const { matchedData } = require('express-validator/filter');
const {OAuth2Client} = require('google-auth-library');
const credentials = require('../credentials.json').web;

module.exports = {
  VerifyUserSession : VerifyUserSession,
  VerifyDeviceNotSelected : VerifyDeviceNotSelected,
  VerifyUserDeviceSession : VerifyUserDeviceSession,
  VerifyDeviceMessageAndSession : VerifyDeviceMessageAndSession
}

async function VerifyUserSession(request, res, next){
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return res.send(Helpers.RequestFailed("Invalid Data Sent"));
  }
  const req = matchedData(request);

  var data = await Helpers.VerifyUserSession(req.id_token,req.session)
                              .catch(console.error);

  if (data) {
    request.SessionData = data;
    next();
  }
  else{
    res.send(Helpers.SessionExpired());
  }
}


async function VerifyUserDeviceSession(request, res, next){
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return res.send(Helpers.RequestFailed("Invalid Data Sent"));
  }
  const req = matchedData(request);

  var data = await Helpers.VerifyUserDeviceSession(req.id_token,req.session,req.deviceId)
                              .catch(console.error);

  if (data) {
    request.SessionData = data;
    next();
  }
  else{
    res.send(Helpers.SessionExpired());
  }
}

async function VerifyDeviceMessageAndSession(request, res, next){
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return res.send(Helpers.RequestFailed("Invalid Data Sent"));
  }
  const req = matchedData(request);

  var data = await Helpers.VerifyUserDeviceSession(req.id_token,req.session,req.deviceId)
                              .catch(console.error);

  if (data) {
    
    let message = await Repository.Messages.findOne({
      "_id" : req.messageId,
      "messagePreference.DeviceId" : req.deviceId
    })

    if (message) {
      data.message = message;
      request.SessionData = data;
      next();
    }
    else{
      res.send(Helpers.SessionExpired());
    }
  }
  else{
    res.send(Helpers.SessionExpired());
  }
}

async function VerifyDeviceNotSelected(request, res, next){
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return res.send(Helpers.RequestFailed("Invalid Data Sent"));
  }
  const req = matchedData(request);

  var verifiedData = await Helpers.VerifyUserSession(req.id_token,req.session)
                              .catch(console.error);

  if (verifiedData) {

    const userData = verifiedData.userData;

    let userAggregate = await Repository.Users.aggregate([{
      $match : {
        Sub : userData.sub,
        "Auth" : {
          $elemMatch : {
            "deviceId" : {
              $exists : true
            },
            "session" : req.session
          }
        }
      }
    },
    {
      $project: {
         Auth: {
            $filter: {
               input: "$Auth",
               as : "auth",
               cond: {}
            }
         }
      }
    }]);
    
    //if userAggregate exists, deviceId is already chosen since deviceId $exists is true
    if (userAggregate.length === 0) {

      let data = {
        userData,
        user : verifiedData.user
      };
      request.SessionData = data;
      next();
    }
    else{
      res.send(Helpers.SessionExpired());
    }
  }
  else{
    res.send(Helpers.SessionExpired());
  }
}
