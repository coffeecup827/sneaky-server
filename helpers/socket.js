const status = require('./status');

function socketHandler(socket){

  console.log("connected");

  socket.on('disconnecting',async function({isFromServer = false}){
    
    if (isFromServer) return;

    await Repository.Users.aggregate([{
      $match : {
        "Auth" : {
          $elemMatch : {
            "socketId" : socket.id
          }
        }
      }
    },{
      $project :{
        "Auth" : {
          $filter: {
            input: "$Auth",
            as : "auth",
            cond: { $eq: [ "$$auth.socketId", socket.id ] }
         }
        }
      }
    }]).then((user) => {

      let deviceId = user[0].Auth[0].deviceId.toString();

      io.of('/').in(deviceId).clients(async (error, clients) => {
        if (error) throw error;

        if (clients.length === 0) {

          await Repository.UserDevices.findOneAndUpdate({
            _id : deviceId
          },{
            IsLoggedIn : false
          })

          let emitData = {
            deviceId : deviceId,
            IsLoggedIn : false
          }
          let messageTemplate = Helpers.StringTemplate`${'deviceName'} Offline`;
          Helpers.NotifyUserDevicesExceptSender('device-status',deviceId,emitData,messageTemplate);
        }
      });
      
    }).then(async () => {
      await Repository.Users.findOneAndUpdate({
        "Auth" : {
          $elemMatch : {
            "socketId" : socket.id
          }
        }
      },{
        $pop : {
          Auth : -1
        }
      });
    })
    .catch(() => {

    })

    console.log('user disconnected');
  });

  socket.on('logout-user',function(){
    socket.disconnect({
      isFromServer : true
    });
    console.log("Logged Out");
  })

  socket.on('join-room',async (params,callback) => {

    var data = await Helpers.VerifyUserDeviceSession(params.id_token,params.session,params.deviceId);
    
    try {

      if (!data) throw error;

      socket.join(data.device._id,async (err) => {

        if (err) {
          throw error;
        }
        else{
          await Repository.UserDevices.findOneAndUpdate({
            _id : data.device._id
          },{
            IsLoggedIn : true
          })

          let emitData = {
            deviceId : data.device._id,
            IsLoggedIn : true
          }
          let messageTemplate = Helpers.StringTemplate`${'deviceName'} Online`;
          Helpers.NotifyUserDevicesExceptSender('device-status',data.device._id,emitData,messageTemplate);

          let emitMessage = {
            "status": status.Success,
            "data":{
              deviceId : data.device._id
            },
            "message": null
          }
          io.to(data.device._id).emit('room-joined', emitMessage);


          callback({
            "status": status.Success,
            "data": null,
            "message": "Connected To Server"
          });
        }
      });
    }
    catch(error){
      callback(JSON.parse(Helpers.RequestFailed("Couldn't Connect To Server")));
    }
    
  })

}

module.exports = socketHandler;