const { cookie,body,checkSchema } = require('express-validator/check');

const id_token_validator = body('id_token').exists().trim().isString().toString();
const current_device_optional_validator = cookie('CurrentDevice').optional().trim().isMongoId();
const socket_id_validator = body('socketId').exists().trim().isString().toString();
const device_id_validator = body("deviceId").exists().trim().isMongoId()
const session_optional_validator = cookie('session').optional().trim().isString().toString();
const session_validator = body('session').exists().trim().isString().toString();

const string_validator = (stringParam) => (body(stringParam).exists().trim().isString().toString());
const mongo_id_validator = (mongoIdParam) => (body(mongoIdParam).exists().trim().isMongoId());
const boolean_validator = (booleanParam) => (body(booleanParam).exists().isBoolean().toBoolean())
const int_validator = (intParam) => (body(intParam).exists().isInt().toInt())

module.exports = {
  SignInValidator : [id_token_validator,current_device_optional_validator,socket_id_validator,session_optional_validator],
  SignOutValidator : [id_token_validator,session_validator],
  GetUserDevicesValidator : [id_token_validator,session_validator],
  CreateDeviceValidator : [id_token_validator,session_validator,
    string_validator('device.DeviceName'),
    body('device.Type').exists().trim().isString().toString().isIn(["web","mobile"])
  ],
  SelectDeviceValidator : [id_token_validator,string_validator("device.DeviceName"),mongo_id_validator("device.DeviceId"),session_validator],
  GetDevicePreferences : [id_token_validator,session_validator,device_id_validator],
  UpdateDevicePreferences : [id_token_validator,mongo_id_validator("preferenceId"),
    boolean_validator("isIncluded"),session_validator,device_id_validator
  ],
  SendMessageValidator : [id_token_validator,session_validator,device_id_validator,string_validator("message")],
  GetMessageValidator : [id_token_validator,session_validator,device_id_validator,int_validator("skipTo")],
  MessageReceivedValidator : [id_token_validator,session_validator,device_id_validator,mongo_id_validator("messageId")]
}
